# Erweiterung der Kontoverwaltung

## Eigene Klasse Kontenverwaltung

Zunächst haben wir in einem kleinen WarmUp die Array-Methoden der letzten Stunde wiederholt, anhand der folgenden Aufgabe:

![WP_20171130_12_44_29_Pro](C:\Users\Gerti\Documents\Schule17\Javascript\Listen\WP_20171130_12_44_29_Pro.jpg)

Wichtig waren hier die Methoden push(), pop(), indexOf() sowie die Eigenschaft count.

Weiter haben Sie folgende Elemente gesammelt, um die Sie die KOntoanwendung erweitern möchten:

![WP_20171130_12_44_36_Pro](C:\Users\Gerti\Documents\Schule17\Javascript\Listen\WP_20171130_12_44_36_Pro.jpg)Folgende Ziele wollen wir zunächst angehen:

1. Die Ausgabenliste schöner gestalten, z.B durch einfügen einer Laufenden Nummer.
2. Eine Methode schreiben die aus einer Liste ein Konto zu einer übergebenen Kontonummer filtert.
3. Das Programm so modifizieren, dass man auszahlen/einzahlen für ein bestimmtes Konto ausführt, für das die Kontonummer angegeben wird.

------

Da mehrere Aktionen für die Verwaltung der Konten benötigt wird, soll die Liste der Konten ebenfalls als eine Klasse eingeführt werden. Denn sonst würde unser Main-Programm auf Dauer sehr unübersichtlich.

Die Oberfläche könnte z.B. zur Berücksichtigung der Ziele so aussehen:

![oberflaeche](C:\Users\Gerti\Documents\Schule17\Javascript\Listen\oberflaeche.JPG)

Der Grundaufbau einer Listenklasse ist sehr einfach:

```js
class KontenVerwaltung  {
    constructor () {
      
        this.konten = [] 
    }
}
```

#### Folgende Umbau-Aufgaben ergeben sich hieraus:

1. Ihrer erste Aufgabe könnte nun darin bestehen, das bisherige Programm so umzubauen, dass die Listenklasse genutzt wird. Hierzu müssten Sie die Klasse oben durch eine toString()-Ausgabemethode erweitern.
2. Auch hier eine addKonto() Methode mit dem Parameter konto zu ergänzen macht Sinn. Denn ein neues Konto sollte nur eingefügt werden, wenn das Konto noch nicht existiert. 
3. Daraus folgt auch, dass Sie eine Methode benötigen, die prüft, ob eine Kontonummer bereits für ein bestehendes Konto vergeben wurde. 

#### Hilfen hierzu

Es gibt mehrere Möglichkeiten, um den Schritt 3 in Javascript zu implementieren: 

1. Mit einer for-Schleife und der indexOf-Methode
2. Mit der find-Methode, die als Argument eine Funktion übergeben bekommt, die den Vergleich für jedes Element des Arrays ausführt.

##### Beispiel zu 2.

```js
let personen = [{name: 'Uwe', age:3}, {name: 'Lisa', age:18} ,{name: 'Georg', age:15}];

function checkErwachsen(person) {
    return person.age >= 18;
}
console.log(personen.find(checkErwachsen )) // liefert das 2. Objekt zurück

```

Dieser Code lässt sich mit Hilfe sogenannter Arrowfunctions (Pfeilfunktionen) noch sehr stark abkürzen:

```
let personen = [{name: 'Uwe', age:3}, {name: 'Lisa', age:18} ,{name: 'Georg', age:15}];


console.log(personen.find(x => x.age >=18) ) // Liefert das 2. Objekt (Lisa) zurück
```

###### Exkurs: Mehr zu  Arrowfunctions, s. Link

http://mfg.fhstp.ac.at/development/ecmascript-6-es6-was-ist-neu-an-der-naechsten-version-von-javascript/#arrow-functions

https://www.ab-heute-programmieren.de/es2015-teil-2-pfeilfunktionen/

