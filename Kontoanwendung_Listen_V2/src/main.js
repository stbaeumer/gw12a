let konto
let kontos = new KontenVerwaltung()
function kontoErzeugen() {
    document.getElementById("ausgabe").innerHTML = ``
    const wahl = document.getElementById("girokonto").checked
    const ktoNr = document.getElementById("ktonr").value
    const ktoInh =  document.getElementById("ktoInh").value
    const saldo = parseFloat(document.getElementById("saldo").value)
    if ( wahl ) {
        const dispo = 2000  // Dies kann später auch als Eingabefeld ergänzt werden
        konto = new GiroKonto(ktoNr, ktoInh, saldo, dispo)
        //
        let text = kontos.addKonto(konto)
        document.getElementById("ausgabe").innerHTML = konto+'<br>'+text
    }
    else{
        const zinsSatz = 0.1 //  Dies kann später auch als Eingabefeld ergänzt werden
     
        konto = new SparKonto(ktoNr, ktoInh, saldo, zinsSatz)
       let text = kontos.addKonto(konto)
       document.getElementById("ausgabe").innerHTML = konto+'<br>'+text
    }
}

function alleKontenAnzeigen() {
    
    document.getElementById("ausgabe").innerHTML = kontos.toString()
}

function ausgeben(kto) {
    document.getElementById("ausgabe").innerHTML = kto.toString()
}

function betragAuslesen() {
    return parseFloat(document.getElementById("betrag").value)
}
function abheben () {
    const betrag = betragAuslesen()
    const bKtoNr = document.getElementById("bktonr").value
    konto = kontos.findKonto(bKtoNr)
    if(konto !== undefined) {
    const istOk = konto.abheben(betrag)  
    if (istOk) {
        ausgeben(konto)
        } else {
            document.getElementById("ausgabe").innerHTML = 'Abhebung nicht möglich'
        }
    } else { 
        document.getElementById("ausgabe").innerHTML = 'Kontonummer existiert nicht'
    }
}
function einzahlen() {
    const betrag = betragAuslesen()
    const bKtoNr = document.getElementById("bktonr").value
    konto = kontos.findKonto(bKtoNr)
    if(konto !== undefined) {
        konto.einzahlen(betrag)
        ausgeben(konto)
    } else {
        document.getElementById("ausgabe").innerHTML = 'Kontonummer existiert nicht'
    }
}
 
document.getElementById("btnKontoErzeugen").addEventListener("click",kontoErzeugen)
document.getElementById("btnAuszahlen").addEventListener("click", abheben)
document.getElementById("btnEinzahlen").addEventListener("click", einzahlen)
document.getElementById("btnAlleKontenAnzeigen").addEventListener("click", alleKontenAnzeigen)

