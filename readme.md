## Projekt Quadrat
In diesem Projekt werden wir uns mit Klassen auseinandersetzen, die helfen, Programme besser zu strukturieren. Hierbei ist es wichtig zu wissen, dass in Javascript mehrere Programmierstile und Konzepte möglich sind. Neben der Objektorientierung ist dies vor allem die funktionale Programmierung. 

Um folgende Begriffe wird es künftig gehen:
- Module
- Klassen
- Objekte
- Konstruktoren
- Methoden
- statische Eigenschaften und MEthoden
- getter und setter

#### Objektoreintierung

Unter Objektorientierung versteht man, dass man Objekte der Wirklichkeit auf die Eigenschaften und das Verhalten reduziert, dass für die geplante Anwendung relevant ist. 
Z. B. werden im Rahmen der digitalrn Kontoführung für ein Konto bestimmte Eigenschaften benötigt, wie Kontostand und Kreditlimit und auch die Möglichkeit Buchungen durchzuführen. Solche Tätigkeiten werden mithilfe von Methoden (ähnlich zu Funktionen) beschrieben.

#### Klassen

Klassen sind **Baupläne** für Objekte. Hier wird beschrieben welche Eigenschaften und Methoden alle gleichartigen Objekte also alle Konten haben sollen. 

#### Objekte

Einzelne nach diesem Bauplan erstellte Vertreter heißen Objekte. Also die ganz konkreten Konten von Hernn Müller, von Frau Meier etc. sind Objekte der Klasse Konto.

#### Konstruktoren

Dies sind spezielle Methoden. Diese geben im Bauplan an, wie genau Objekte gebaut werden können und welche Informationen hierfür benötigt werden.

### Beispiel - geometrische Figuren

``` js
class Quadrat {
    constructor(laenge, farbe) {
        this.farbe = farbe
        this.laenge = laenge
    }
        flaecheBerechnen() {
         return this.laenge * this.laenge

    }
}
```

Klassenbezeichner beginnen i.d.R. mit einem Großbuchstaben.
Im Konstruktor werden die Eigenschaften festgelegt. Hier sind dies die
Farbe und Länge (Seitenlänge) des Quadrats. Das Schlüsselwort this kennzeichnet hier im Bauplan stellvertretend das jeweils aktuelle  Objekt. Überall in der Klasse können (und müssen) diese Eigenschaften nun mit this. verwendet werden. Außerhalb der Klasse kommt man nicht an diese Eigenschaften heran. (Probieren Sie es selbst einmal aus.)

#### Konstruktoraufruf 

Neue Objekte der Klasse werden mit Hilfe des Konstruktors gebildet. Dieser wird ähnlich wie eine Funktion aufgerufen aber verbunden mit dem Schlüsselwort **new**.
``` js

let quadrat = new Quadrat(5, "green")
```
#### Auf Objekte zugreifen und Objekte ändern

Das kennen wir im Prinzip schon. Wir erreichen die Eigenschaften und Methoden immer mithilfe des Objektbezeichners kombiniert mit dem Punktselektor und der zugehörigen Eigenschaft bzw. Methode.
Was erwarten Sie, was folgende Codezeilen bewirken?

``` js
const quadrat = new Quadrat(10, "red")
console.log(quadrat.laenge)
console.log(quadrat.flaecheBerechnen())
//
quadrat.laenge=12
console.log(quadrat.flaecheBerechnen())

```
Nun soll es auch eine kleien Webanwendung dazu geben:



![Formular](/bilder/QuadratForm.JPG)

### Ihre Aufgabe

Das Quadrat ändern klappt schon, aber leider wird bisher nur die Fläche berechnet und angezeigt. Ergänzen Sie in der Klasse eine entsprechende Methode zum Umfang berechnen und ergänzen Sie die Ausgabe entsprechend. 

Und dann gibt es noch ein Button zum Zeichnen des Quadrats. 
Und da es schon ein bisschen her ist, dass wir mit Canvas & Co unterwegs waren, hilft vielleicht dieser Link hier weiter:
[Tutorial zum Zeichnen von Rechtecken und Kreisen](https://www.askingbox.de/tutorial/html5-canvas-einsteiger-tutorial-teil-3-rechtecke-und-kreise)



Viel Spaß beim Ausprobieren!