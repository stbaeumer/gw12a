## Listen

### Listen und Konto
Bislang können Sie auf ein Konto einzahlen und das Konto auch anzeigen.
In der realen Welt ist das nicht ausreichend. Typischerweise gibt es bei einer Bank 
_zahlreiche_ Konten, Kunden, Mitarbeiter usw.

### Listen in Javascript

In Javascript ist das _ARRAY_ das Mittel der Wahl zur Verwaltung von Objekten in Listen. Das Array selbst ist dabei auch ein Objekt, das wie jedes Objekt instanziiert werden muss:

```JS
// Instanziierung eines Objekts vom Typ Array
let meinBuchArray = []
```

Bemerkenswert ist hier die minimalistische Syntax. Die Instanziierung kommt bei Arrays ohne das reservierte Wort *new* aus. Ein Array enthält Objekte mit eigenen Methoden und Eigenschaften und bringt seinerseits Eigenschaften und Methoden mit:

```JS
// Methode zum Hinzufügen eines Objekts
meinBuchArray.push(einBuch)

// Eigenschaft, die die Anzahl der Elemente im Array zurückgibt
meinBuchArray.length // 1
```

Wer es noch kürzer mag, kann obige Anweisungen noch weiter verdichten:

```JS
let meinBuchArray = [einBuch, nochEinBuch]
meinBuchArray.length // 2
```

Ein Array kann man sich wie einen Stapel vorstellen. Jedes weitere *gepushte* Objekt wird oben auf den Stapel gelegt. Damit jedes Objekt im Stapel einzeln herausgesucht werden kann, werden alle Objekte von unten nach oben abgezählt, wobei bei Null angefangen wird zu zählen. Das erste dem Stapel hinzugefügte Objekt hat also den Index 0. Das zweite Objekt hat den Index 1 usw.

```JS
let titelDesErstenBuches = meinBuchArray[0].titel 
let autorDesZweitenBuches = meinBuchArray[1].autor 
let titelDesZuletztHinzugefügtenBuches = meinBuchArray[meinBuchArray.length - 1].titel 
console.log(meinBuchArray)
```

Eine besondere Bedeutung bei der Verarbeitung von Listen kommt den Schleifen zu. Wenn alle Bücher einer Liste angezeigt werden sollen, dann ist das Sache einer Schleife:

```JS
let ausgabe = ""
for (var i = 0; i < meinBuchArray.length; i++) {
    ausgabe += meinBuchArray[i].toString() + "<br>"
}
ausgabe += "----------<br>Anzahl: " + meinBuchArray.length
document.getElementById("ausgabe").innerHTML = ausgabe
```


### Aufgabe

Erweitern Sie Ihre Kontoverwaltung um die Funktionalität einer Liste! Es sollte möglich sein mehrere Konton anzulegen und anzuzeigen.




