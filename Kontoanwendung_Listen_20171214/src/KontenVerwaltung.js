class KontenVerwaltung  {
    constructor () {      
        this.konten = [] 
    }
   
    addKonto(kto) {
        if (this.konten.find( x => x.ktoNr===kto.ktoNr)===undefined)  {  // undefined wird zu false konvertiert
            this.konten.push(kto)
            return 'Konto wurde angefügt'
        }
        else {
            return 'Kontonummer ist bereits vergeben, Konto wurde also nicht angefügt.'
        }
    }
    
    // Gibt das Konto zurück, falls vorhanden, sonst den Wert undefined
    findKonto(ktoNr) {
        return this.konten.find( x => x.ktoNr === ktoNr)
    }
    toString() {
        let ausgabe = ""
        let kto;
        
        for (var i = 0; i < this.konten.length; i++) {
                       
            ausgabe += this.konten[i].toString() + "<br>"
        }
        ausgabe += "----------<br>Anzahl: " + this.konten.length
       return ausgabe
    }
}