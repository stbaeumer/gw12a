## Kontoanwendung

bild einfügen Kontoauszug / Sparbuch

Situationsbeschreibung - Spaßkasse BKBor (oder auch gerne seriöser) möchte den Schülerinen und Schülern das Führen eines Kontos online ermöglichen.  ...

(Da haben wir uns natürlich gleich eine schwierige Anwendung in Sachen Sicherheit von Daten ausgesucht!!!)

## 1. Aufschlag

![Kontostart_Oberflaeche](C:\Users\Gerti\Documents\Notitzen\Bilder\Kontostart_Oberflaeche.JPG)



```html
   <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="style/style.css" />
    
</head>
<body>
<h4>Kontoführung</h4>
<form>
<input type="radio" name="form" id="girokonto" >Girokonto <br>
<input type="radio" name="form" id="sparkonto" checked>Sparkonto <br>
</form>
<div>
<label for="kontonummer"> Kontonummer: </label>
<input type="text" name="kontonummer" id="ktonr" placeholder="50607080" value="50607080">
</div>
<div>
<label for="kontoinhaber">Kontoinhaber: </label>
<input type="text" name="kontoinhaber" id="ktoInh" placeholder="Mustermann" value="Mustermann">
</div>
<br>
<label for="saldo">Betrag zur Kontoeröffnung:   </label>
<input type="text" name="saldo" id="saldo" placeholder="0" value="0">
<br>
<br>
<button id="btnKontoErzeugen">Konto erzeugen</button>
<br>
<div>
    <label for="betrag">Ein-/Auszahlungsbetrag: </label>
    <input type="text" name="betrag" id="betrag" placeholder="0" value="0">
    </div>
    <br>
<button id="btnAuszahlen">Betrag abheben</button>
<br>
<button id="btnEinzahlen">Betrag einzahlen</button>
<div id="ausgabe"></div>
</body>
<script src="src/konto.js">
</script>
<script src="src/girokonto.js">
</script>
<script src="src/sparkonto.js">
</script>
<script src="src/main.js">
   
</script>

</html>
```
Style ist noch höchst mager!

```css
label {
    width: 10em;
    display: block;
    float: left;
}
input {width: 80px}
button {width: 200px}
body {
    padding: 15px 5px;
    font-family: Verdana;
}
```



### Klassen

```js
class Konto {
    constructor(ktoNr, ktoInhaber, ktoSaldo) {
        this.ktoNr= ktoNr
        this.ktoInhaber = ktoInhaber
        this.ktoSaldo = ktoSaldo
    }
    abheben(betrag) {
         this.ktoSaldo -= betrag
         return true
    }
    einzahlen(betrag) {
        this.ktoSaldo += betrag
   }
   toString() {
       return `Kontonummer: ${this.ktoNr} <br>
               Kontoinhaber: ${this.ktoInhaber} <br>
               Saldo: ${this.ktoSaldo.toString()}`
   }
}

class SparKonto extends Konto {
    constructor(ktoNr, ktoInhaber, ktoSaldo, zinsSatz) {
        super(ktoNr, ktoInhaber, ktoSaldo)
        this.zinsSatz = zinsSatz
    }
      abheben (betrag) {
          if (this.ktoSaldo - betrag >= 0 ) {
                super.abheben(betrag)
                return true
            } else {
                return false
            }
      }  
      toString() {
          return super.toString() + `<br> Zinssatz: ${this.zinsSatz.toString()} %`
      }
}

class GiroKonto extends Konto {
    constructor(ktoNr, ktoInhaber, ktoSaldo, dispo) {
        super(ktoNr, ktoInhaber, ktoSaldo)
        this.dispo = dispo
    }
      abheben (betrag) {
          if (this.ktoSaldo - betrag >= - this.dispo ) {
                super.abheben(betrag)
                return true
            } else {
                return false
            }
      }  
      toString() {
        return super.toString() + `<br> Dispositionskredit: ${this.dispo.toString()}`
    }
}
```

### Und die Main-Anwendung:

```js
let konto
function kontoErzeugen() {
    document.getElementById("ausgabe").innerHTML = ``
    const wahl = document.getElementById("girokonto").checked
    const ktoNr = document.getElementById("ktonr").value
    const ktoInh =  document.getElementById("ktoInh").value
    const saldo = parseFloat(document.getElementById("saldo").value)
    if ( wahl ) {
        const dispo = 2000  // Dies kann später auch als Eingabefeld ergänzt werden
        konto = new GiroKonto(ktoNr, ktoInh, saldo, dispo)
        console.log(konto)
        
    }
    else{
        const zinsSatz = 0.1 //  Dies kann später auch als Eingabefeld ergänzt werden
     
        konto = new SparKonto(ktoNr, ktoInh, saldo, zinsSatz)
        console.log(konto)
    }
    ausgeben(konto)
}

function ausgeben(kto) {
    document.getElementById("ausgabe").innerHTML = kto.toString()
}

function betragAuslesen() {
    return parseFloat(document.getElementById("betrag").value)
}
function abheben () {
    const betrag = betragAuslesen()
    const istOk = konto.abheben(betrag)  
    if (istOk) {
    ausgeben(konto)
    } else {
        document.getElementById("ausgabe").innerHTML = 'Abhebung nicht möglich'
    }
    console.log(konto)
}
function einzahlen() {
    const betrag = betragAuslesen()
    konto.einzahlen(betrag)
    ausgeben(konto)
}
 
document.getElementById("btnKontoErzeugen").addEventListener("click",kontoErzeugen)
document.getElementById("btnAuszahlen").addEventListener("click", abheben)
document.getElementById("btnEinzahlen").addEventListener("click", einzahlen)

```

### Ideen für weitere Schritte:

getter / setter 

dispo, sparzins über Oberfläche eingeben

Methode zur Zinsberechnung



### und Später

Arays, Arrays, viele Konten

