// eine geometrische Form.
class Shape {
    constructor (context, x, y, color) {        
        this.context = context
        this.color = color
        this.ctx = context.getCtx()
        this.setX(x)
        this.setY(y)
    }
    setX (x) {
        this.x = x
        this.coordX = this.context.scaleXtoPixelX(this.x)
    }
    setY (y) {
        this.y = y
        this.coordY = this.context.scaleYtoPixelY(this.y)
    }
    set (x, y) {
        this.setX(x)
        this.setY(y)
    }
    draw (context) {
        console.log('in der abgeleiteten Klasse zu implementieren')
    }
    computeArea() {
        console.log('Flächenberechnung in der abgeleiteten Klasse zu implementieren')
    }
    computePerimeter() {
        console.log('Umfangberechnung in der abgeleiteten Klasse zu implementieren')
    }
    }
// a shape centered around (x, y)
//    (x, y) refer to the size of the ScaledContext
//    (measured in some unit such as cm)
