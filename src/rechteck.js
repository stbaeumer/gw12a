class Rechteck extends Shape {
    constructor (context, x, y, color, sideX, sideY) {
        super(context, x, y, color)
        this.setSideX(sideX)
        this.setSideY(sideY)
    }
    setSideX (sideX) {
        this.sideX = sideX
        this.lenX = this.context.scaleXtoPixelX(this.sideX)
    }
    setSideY (sideY) {
        this.sideY = sideY
        this.lenY = this.context.scaleYtoPixelY(this.sideY)
    }
    draw () {
        this.ctx.beginPath()
        // canvas ctx.rect(x, y, width, height)
        //   (x,y) is the upper left corner
        this.ctx.rect(this.coordX - this.lenX / 2,
            this.coordY - this.lenY / 2,
            this.lenX, this.lenY)
        this.ctx.stroke()
    }

    computeArea () {
        return this.sideX * this.sideY
    }
 
    computePerimeter () {
        return 2 * (this.sideX + this.sideY)
    }
}
