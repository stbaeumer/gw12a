var arbeitnehmer
var arbeiter
var maschinenführer

class Arbeitnehmer{
    constructor (n,a) {
        this.name = n
        this.abteilung = a
    }

    getAAA()
    {
        return this.name + this.abteilung;
    }
}

class Arbeiter extends Arbeitnehmer{
    constructor(n,a,p) {
        super(n,a)
        this.projekte = p
    }
}

class Maschinenführer extends Arbeiter{
    constructor(n,a,p,m) {
        super(n,a,p)
        this.maschine = m
    }
}

arbeitnehmer = new Arbeitnehmer("Meyer","Abteilung1");
console.log(arbeitnehmer)


arbeiter = new Arbeiter("Müller", "Produktion", "Projekt1")
console.log(arbeiter)

maschinenführer = new Maschinenführer("Schmidt","Abteilung1", "Projekt1", "Maschine1")



console.log(maschinenführer)

console.log(maschinenführer.getAAA());