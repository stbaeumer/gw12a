const scaledCtx = new ScaledContext('canvas', 10, 8)
var quadrat
function quadratErzeugen() {
    const laenge = parseFloat(document.getElementById("kl").value)
    const xPos = parseFloat(document.getElementById("x-pos").value)
    const yPos = parseFloat(document.getElementById("y-pos").value)
    const farbe = document.getElementById("farbe").value
    quadrat = new Quadrat(scaledCtx,xPos,yPos, farbe, laenge)
    console.log(quadrat)
}

function ausgeben() {
    document.getElementById("ausgabe").innerHTML = `Die Fläche beträgt ${quadrat.computeArea().toString()} cm<sup>2</sup>`
}

function zeichnen() {
    quadrat.draw()
}
 
document.getElementById("btn-quadrat-erzeugen").addEventListener("click",quadratErzeugen)
document.getElementById("btn-berechnen").addEventListener("click", ausgeben)
document.getElementById("btn-zeichnen").addEventListener("click", zeichnen)
