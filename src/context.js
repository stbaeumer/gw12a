class ScaledContext {
    constructor (eleId, sizeX, sizeY) {
        this.ele = document.getElementById(eleId)
        this.ctx = this.ele.getContext("2d")

        this.height = this.ctx.canvas.clientHeight
        this.width  = this.ctx.canvas.clientWidth

        this.sizeX = sizeX
        this.sizeY = sizeY

        // change drawing color if needed
        //         this.ctx.strokeStyle = 'red'
    }
    // converts a measurement to pixels:
    // e.g. a width of 2 [cm] to the corresponding size in
    // pixels given the scale of the whole canvas (e.g. 5 [cm])
    //    example: canvas width 300 should equal 5 cm
    //    hence 2 cm correspond to 300 * 2 / 5 = 120 pixels
    scaleXtoPixelX (sX) {
        return this.width * sX / this.sizeX
    }
    scaleYtoPixelY (sY) {
        return this.height * sY / this.sizeY
    }
    // context for drawing
    getCtx () {
        return this.ctx
    }
    clear () {
        this.ctx.clearRect(0, 0, this. width, this.height)
    }

}
