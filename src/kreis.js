class Kreis extends Shape {
    constructor (x, y, radius, color) {
        super(x, y, color)
        this.radius = radius
    }
    draw (context) {
        const ctx = context.getCtx()
        
        const coordX = context.scaleXtoPixelX(this.x)
        const coordY = context.scaleYtoPixelY(this.y)
        const radX = context.scaleXtoPixelX(this.radius)
        const radY = context.scaleYtoPixelY(this.radius)
        
        ctx.beginPath()
        ctx.ellipse(coordX, coordY,
                   radX, radY,
                   0, 0, 2 * Math.PI)
        ctx.stroke()        
    }
}